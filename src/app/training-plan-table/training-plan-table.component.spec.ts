import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingPlanTableComponent } from './training-plan-table.component';

describe('TrainingPlanTableComponent', () => {
  let component: TrainingPlanTableComponent;
  let fixture: ComponentFixture<TrainingPlanTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingPlanTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingPlanTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
