import { ExerciseData } from './../class/exercise-data';
import { ExerciseService } from './../service/exercise.service';
import { TrainingData } from './../class/training-data';
import { Component, OnInit } from '@angular/core';
import { TrainingService } from '../service/training.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TrainingDialogComponent } from '../training-dialog/training-dialog.component';
import { Router, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-training-plan-table',
  templateUrl: './training-plan-table.component.html',
  styleUrls: ['./training-plan-table.component.css']
})
export class TrainingPlanTableComponent implements OnInit {
  trainings: TrainingData[] = [];
  exercises: ExerciseData[] = [];
  duplicatedExercises: ExerciseData[] = [];
  mondayTrainings: TrainingData[] = [];
  tuesdayTrainings: TrainingData[] = [];
  wednesdayTrainings: TrainingData[] = [];
  thursdayTrainings: TrainingData[] = [];
  fridayTrainings: TrainingData[] = [];
  saturdayTrainings: TrainingData[] = [];
  sundayTrainings: TrainingData[] = [];
  MondayDate: Date;
  TuesdayDate: Date;
  WednesdayDate: Date;
  ThursdayDate: Date;
  FridayDate: Date;
  SaturdayDate: Date;
  SundayDate: Date;
  k = 0;
  dataString1: string;
  dataString2: string;

  constructor(
    private trainingService: TrainingService,
    private exerciseService: ExerciseService,
    private dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void {
    this.initDates();
    this.dataString1 = this.MondayDate.toISOString();
    this.dataString1 = this.dataString1.substr(0, 10);
    this.dataString2 = this.SundayDate.toISOString();
    this.dataString2 = this.dataString2.substr(0, 10);
    this.trainingService.getAllTrainings(this.dataString1, this.dataString2).subscribe(
      data => { this.trainings = data; });
    let dataString = this.MondayDate.toISOString();
    dataString = dataString.substr(0, 10);
    this.trainingService.getDayTrainings(dataString).subscribe(
      data => { this.mondayTrainings = data; });
    dataString = this.TuesdayDate.toISOString();
    dataString = dataString.substr(0, 10);
    this.trainingService.getDayTrainings(dataString).subscribe(
      data => { this.tuesdayTrainings = data; });
    dataString = this.WednesdayDate.toISOString();
    dataString = dataString.substr(0, 10);
    this.trainingService.getDayTrainings(dataString).subscribe(
      data => { this.wednesdayTrainings = data; });
    dataString = this.ThursdayDate.toISOString();
    dataString = dataString.substr(0, 10);
    this.trainingService.getDayTrainings(dataString).subscribe(
      data => { this.thursdayTrainings = data; });
    dataString = this.FridayDate.toISOString();
    dataString = dataString.substr(0, 10);
    this.trainingService.getDayTrainings(dataString).subscribe(
      data => { this.fridayTrainings = data; });
    dataString = this.SaturdayDate.toISOString();
    dataString = dataString.substr(0, 10);
    this.trainingService.getDayTrainings(dataString).subscribe(
      data => { this.saturdayTrainings = data; });
    dataString = this.SundayDate.toISOString();
    dataString = dataString.substr(0, 10);
    this.trainingService.getDayTrainings(dataString).subscribe(
      data => { this.sundayTrainings = data; });
  }

  changeWeekMinus(): void {
    this.k--;
    this.ngOnInit();
  }

  changeWeekPlus(): void {
    this.k++;
    this.ngOnInit();
  }

  openDialog(i, day): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '450px';
    const date = new Date(day);
    let t;
    switch (date.getDay()) {
      case 1:
        t = this.mondayTrainings[i];
        dialogConfig.data = {
          training: this.mondayTrainings[i]
        };
        break;
      case 2:
        t = this.tuesdayTrainings[i];
        dialogConfig.data = {
          training: this.tuesdayTrainings[i]
        };
        break;
      case 3:
        t = this.wednesdayTrainings[i];
        dialogConfig.data = {
          training: this.wednesdayTrainings[i]
        };
        break;
      case 4:
        t = this.thursdayTrainings[i];
        dialogConfig.data = {
          training: this.thursdayTrainings[i]
        };
        break;
      case 5:
        t = this.fridayTrainings[i];
        dialogConfig.data = {
          training: this.fridayTrainings[i]
        };
        break;
      case 6:
        t = this.saturdayTrainings[i];
        dialogConfig.data = {
          training: this.saturdayTrainings[i]
        };
        break;
      case 0:
        t = this.sundayTrainings[i];
        dialogConfig.data = {
          training: this.sundayTrainings[i]
        };
        break;
    }
    const dialogRef = this.dialog.open(TrainingDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      switch (value) {
        case 'd':
          this.trainingService.getAllTrainings(this.dataString1, this.dataString2).subscribe(
            data => { this.trainings = data; });
          switch (date.getDay()) {
            case 1:
              this.mondayTrainings.splice(i, 1);
              break;
            case 2:
              this.tuesdayTrainings.splice(i, 1);
              break;
            case 3:
              this.wednesdayTrainings.splice(i, 1);
              break;
            case 4:
              this.thursdayTrainings.splice(i, 1);
              break;
            case 5:
              this.fridayTrainings.splice(i, 1);
              break;
            case 6:
              this.saturdayTrainings.splice(i, 1);
              break;
            case 0:
              this.sundayTrainings.splice(i, 1);
              break;
          }
          break;
        case 'e':
          this.router.navigate(['/dodaj-trening'], { queryParams: t });
          break;
        case 'x':
          break;
        default:
          let exerciseId;
          const newTraining = new TrainingData(value.id, value.name, value.date, value.startTime, value.endTime);
          console.log(value.id);
          this.trainingService.addTraining(newTraining).subscribe(
            () => {
              this.exerciseService.getExercises(t.id).subscribe(
                exercises => {
                  this.exercises = exercises;
                  this.exerciseService.getExerciseId().subscribe((data) => {
                    exerciseId = data + 1;
                    this.duplicatedExercises = this.exercises;
                    for (i = 0; i < this.duplicatedExercises.length; i++) {
                      this.duplicatedExercises[i].id = exerciseId;
                      console.log(this.duplicatedExercises[i].id);
                      this.duplicatedExercises[i].trainingID = value.id;
                      this.exerciseService.addExercise(this.duplicatedExercises[i]).subscribe();
                      exerciseId++;
                    }
                  });
                });
              this.loadData();
            });
          break;
      }
    });
  }

  initDates(): void {
    let t = new Date();
    const DayInMilis = 86400000;
    if (this.k !== 0) {
      t = new Date(t.getTime() + 7 * this.k * DayInMilis);
    }
    let dayOfTheWeek;
    dayOfTheWeek = t.getDay();
    switch (dayOfTheWeek) {
      case 0:
        this.SundayDate = t;
        this.SaturdayDate = new Date(t.getTime() - DayInMilis);
        this.FridayDate = new Date(t.getTime() - 2 * DayInMilis);
        this.ThursdayDate = new Date(t.getTime() - 3 * DayInMilis);
        this.WednesdayDate = new Date(t.getTime() - 4 * DayInMilis);
        this.TuesdayDate = new Date(t.getTime() - 5 * DayInMilis);
        this.MondayDate = new Date(t.getTime() - 6 * DayInMilis);
        break;
      case 1:
        this.MondayDate = t;
        this.TuesdayDate = new Date(t.getTime() + DayInMilis);
        this.WednesdayDate = new Date(t.getTime() + 2 * DayInMilis);
        this.ThursdayDate = new Date(t.getTime() + 3 * DayInMilis);
        this.FridayDate = new Date(t.getTime() + 4 * DayInMilis);
        this.SaturdayDate = new Date(t.getTime() + 5 * DayInMilis);
        this.SundayDate = new Date(t.getTime() + 6 * DayInMilis);
        break;
      case 2:
        this.TuesdayDate = t;
        this.MondayDate = new Date(t.getTime() - DayInMilis);
        this.WednesdayDate = new Date(t.getTime() + 1 * DayInMilis);
        this.ThursdayDate = new Date(t.getTime() + 2 * DayInMilis);
        this.FridayDate = new Date(t.getTime() + 3 * DayInMilis);
        this.SaturdayDate = new Date(t.getTime() + 4 * DayInMilis);
        this.SundayDate = new Date(t.getTime() + 5 * DayInMilis);
        break;
      case 3:
        this.WednesdayDate = t;
        this.MondayDate = new Date(t.getTime() - 2 * DayInMilis);
        this.TuesdayDate = new Date(t.getTime() - 1 * DayInMilis);
        this.ThursdayDate = new Date(t.getTime() + 1 * DayInMilis);
        this.FridayDate = new Date(t.getTime() + 2 * DayInMilis);
        this.SaturdayDate = new Date(t.getTime() + 3 * DayInMilis);
        this.SundayDate = new Date(t.getTime() + 4 * DayInMilis);
        break;
      case 4:
        this.ThursdayDate = t;
        this.WednesdayDate = new Date(t.getTime() - DayInMilis);
        this.TuesdayDate = new Date(t.getTime() - 2 * DayInMilis);
        this.MondayDate = new Date(t.getTime() - 3 * DayInMilis);
        this.FridayDate = new Date(t.getTime() + DayInMilis);
        this.SaturdayDate = new Date(t.getTime() + 2 * DayInMilis);
        this.SundayDate = new Date(t.getTime() + 3 * DayInMilis);
        break;
      case 5:
        this.FridayDate = t;
        this.ThursdayDate = new Date(t.getTime() - DayInMilis);
        this.WednesdayDate = new Date(t.getTime() - 2 * DayInMilis);
        this.TuesdayDate = new Date(t.getTime() - 3 * DayInMilis);
        this.MondayDate = new Date(t.getTime() - 4 * DayInMilis);
        this.SaturdayDate = new Date(t.getTime() + DayInMilis);
        this.SundayDate = new Date(t.getTime() + 2 * DayInMilis);
        break;
      case 6:
        this.SaturdayDate = t;
        this.FridayDate = new Date(t.getTime() - DayInMilis);
        this.ThursdayDate = new Date(t.getTime() - 2 * DayInMilis);
        this.WednesdayDate = new Date(t.getTime() - 3 * DayInMilis);
        this.TuesdayDate = new Date(t.getTime() - 4 * DayInMilis);
        this.MondayDate = new Date(t.getTime() - 5 * DayInMilis);
        this.SundayDate = new Date(t.getTime() + DayInMilis);
        break;
    }
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
