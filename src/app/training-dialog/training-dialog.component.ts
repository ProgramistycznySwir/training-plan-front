import { DuplicateTrainingComponent } from './../duplicate-training/duplicate-training.component';
import { TrainingService } from './../service/training.service';
import { TrainingData } from './../class/training-data';
import { ExerciseService } from './../service/exercise.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { ExerciseData } from '../class/exercise-data';

@Component({
  selector: 'app-training-dialog',
  templateUrl: './training-dialog.component.html',
  styleUrls: ['./training-dialog.component.css']
})
export class TrainingDialogComponent implements OnInit {
  trainingName: string;
  training: TrainingData;
  duplicatedTraining: TrainingData;
  exercises: ExerciseData[] = [];

  constructor(
    private exerciseService: ExerciseService,
    private trainingService: TrainingService,
    private dialogRef: MatDialogRef<TrainingDialogComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) data) {
    this.training = data.training;
  }

  ngOnInit(): void {
    this.trainingName = this.training.name;
    this.exerciseService.getExercises(this.training.id).subscribe(
      receivedExercises => { this.exercises = receivedExercises; });
  }

  edit() {
    this.dialogRef.close('e');
  }

  duplicateTraining() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '300px';
    dialogConfig.data = {
      training: this.training
    };

    const dialogRef = this.dialog.open(DuplicateTrainingComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.dialogRef.close(data);
      }
    });
  }

  close() {
    this.dialogRef.close('x');
  }

  delete() {
    if (confirm('Jesteś pewien że chcesz usunąć trening?')) {
      this.trainingService.deleteTraining(this.training.id).subscribe();
      this.exerciseService.deleteAllExercisesWithTrainingId(this.training.id).subscribe();
      this.dialogRef.close('d');
    }
  }

  exerciseDone(i) {
    this.exercises[i].status = 1;
    this.exerciseService.addExercise(this.exercises[i]).subscribe();
  }

  exerciseFailed(i) {
    this.exercises[i].status = 2;
    this.exerciseService.addExercise(this.exercises[i]).subscribe();
  }
}
