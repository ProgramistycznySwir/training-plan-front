import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ExerciseData } from './../class/exercise-data';

@Injectable({
  providedIn: 'root'
})
export class ExerciseService {

  constructor(private http: HttpClient) { }

  public addExercise(exercise: ExerciseData): Observable<ExerciseData> {
    return this.http.post<ExerciseData>('http://localhost:8080/add-exercise', exercise);
  }

  public getExerciseId(): Observable<any> {
    return this.http.get('http://localhost:8080/get-exercise-id');
  }

  public getExercises(trainingId: number): Observable<ExerciseData[]> {
    return this.http.get<ExerciseData[]>('http://localhost:8080/get-exercises?trainingId=' + trainingId);
  }

  public getAllExercises(): Observable<ExerciseData[]> {
    return this.http.get<ExerciseData[]>('http://localhost:8080/get-all-exercises');
  }

  public deleteExercise(exerciseId: number): Observable<any> {
    return this.http.delete('http://localhost:8080/delete-exercise?exerciseId=' + exerciseId);
  }

  public deleteAllExercisesWithTrainingId(trainingId: number): Observable<any> {
    return this.http.delete('http://localhost:8080/delete-all-exercises-with-training-id?trainingId=' + trainingId);
  }

  public getDistinctExercisesName(): Observable<ExerciseData[]> {
    return this.http.get<ExerciseData[]>('http://localhost:8080/get-distinct-exercises-name');
  }
}
