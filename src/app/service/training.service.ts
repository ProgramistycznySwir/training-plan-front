import { TrainingPlanTableComponent } from './../training-plan-table/training-plan-table.component';
import { TrainingData } from './../class/training-data';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {

  constructor(private http: HttpClient) { }

  public getAllTrainings(monday: string, sunday: string): Observable<TrainingData[]> {
    return this.http.get<TrainingData[]>('http://localhost:8080/get-all-trainings?monday=' + monday + '&sunday=' + sunday);
  }

  public getDayTrainings(date: string): Observable<TrainingData[]> {
    return this.http.get<TrainingData[]>('http://localhost:8080/get-day-trainings?date=' + date);
  }

  public getTrainingsById(trainingID: number[]): Observable<TrainingData[]> {
    return this.http.get<TrainingData[]>('http://localhost:8080/get-trainings-by-id?trainingID=' + trainingID);
  }

  public getTrainingId(): Observable<any> {
    return this.http.get('http://localhost:8080/get-training-id');
  }

  public addTraining(training: TrainingData): Observable<TrainingData> {
    return this.http.post<TrainingData>('http://localhost:8080/add-training', training);
  }

  public deleteTraining(trainingId: number): Observable<any> {
    return this.http.delete('http://localhost:8080/delete-training?trainingId=' + trainingId);
  }

}
