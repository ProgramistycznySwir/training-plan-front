export class TrainingData {
  id: number;
  name: string;
  date: string;
  startTime: string;
  endTime: string;

  constructor(id, name, date, startTime, endTime) {
    this.id = id;
    this.name = name;
    this.date = date;
    this.startTime = startTime;
    this.endTime = endTime;
  }
}
