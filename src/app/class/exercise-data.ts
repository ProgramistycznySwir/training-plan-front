export class ExerciseData {
  id: number;
  name: string;
  series: number;
  reps: number;
  weight: number;
  trainingID: number;
  status: number;

  public constructor(id, name, series, reps, weight, trainingID, status) {
    this.id = id;
    this.name = name;
    this.series = series;
    this.reps = reps;
    this.weight = weight;
    this.trainingID = trainingID;
    this.status = status;
  }
}
