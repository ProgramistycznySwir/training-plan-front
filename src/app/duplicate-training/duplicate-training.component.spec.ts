import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuplicateTrainingComponent } from './duplicate-training.component';

describe('DuplicateTrainingComponent', () => {
  let component: DuplicateTrainingComponent;
  let fixture: ComponentFixture<DuplicateTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuplicateTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuplicateTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
