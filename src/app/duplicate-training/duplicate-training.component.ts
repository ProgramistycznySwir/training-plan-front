import { TrainingService } from './../service/training.service';
import { TrainingData } from './../class/training-data';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TrainingDialogComponent } from '../training-dialog/training-dialog.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-duplicate-training',
  templateUrl: './duplicate-training.component.html',
  styleUrls: ['./duplicate-training.component.css']
})
export class DuplicateTrainingComponent implements OnInit {

  trainingForm: FormGroup;
  submittedTrainingForm = false;
  training: TrainingData;
  trainingId = 0;
  constructor(
    private trainingService: TrainingService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<TrainingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.training = data.training;
  }

  ngOnInit(): void {
    this.trainingForm = this.formBuilder.group({
      date: ['', Validators.required],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required]
    });
  }

  duplicateTraining(date, startTime, endTime): void {
    this.submittedTrainingForm = true;

    if (this.trainingForm.invalid) {
      return;
    }
    let t;
    this.trainingService.getTrainingId()
      .subscribe((data) => {
        this.trainingId = data + 1;
        t = new TrainingData(this.trainingId, this.training.name, date, startTime, endTime);
        this.dialogRef.close(t);
      });
  }

  get t() {
    return this.trainingForm.controls;
  }

  close() {
    this.dialogRef.close();
  }
}
