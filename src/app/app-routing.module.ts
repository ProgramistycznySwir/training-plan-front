import { StatisticsComponent } from './statistics/statistics.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTrainingComponent } from './add-training/add-training.component';
import { TrainingPlanTableComponent } from './training-plan-table/training-plan-table.component';


const routes: Routes = [
  {path: '', component: TrainingPlanTableComponent},
  {path: 'dodaj-trening', component: AddTrainingComponent, data: { animation: 'isRight'}},
  {path: 'statistics', component: StatisticsComponent, data: { animation: 'isLeft'}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
