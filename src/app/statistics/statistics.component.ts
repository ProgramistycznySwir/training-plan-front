import { TrainingService } from './../service/training.service';
import { TrainingData } from './../class/training-data';
import { ExerciseData } from './../class/exercise-data';
import { Component, OnInit, Pipe, PipeTransform, forwardRef } from '@angular/core';
import { ExerciseService } from '../service/exercise.service';
import { Chart } from './Chart.js';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  exercises: ExerciseData[] = [];
  filtredExercises: ExerciseData[] = [];
  exercisesWithSameName: ExerciseData[] = [];
  trainings: TrainingData[] = [];
  exerciseChose: boolean[] = [];
  exerciseChosen = false;
  previewExerciseChosen = 0;
  viewChart = false;
  chart: Chart;
  ctx: HTMLElement;

  constructor(
    private exerciseService: ExerciseService,
    private trainingService: TrainingService) { }

  ngOnInit(): void {
    this.exerciseService.getAllExercises().subscribe(
      receivedExercises => {
        this.exercises = receivedExercises;
      });
    this.exerciseService.getDistinctExercisesName().subscribe(
      receivedExercises => {
        this.filtredExercises = receivedExercises;
        for (let i = 0; i < this.filtredExercises.length; i++) {
          this.exerciseChose[i] = false;
        }
      });
    this.ctx = document.getElementById('myChart');
  }

  checkProgress(i): void {
    const a = Array<number>();
    this.trainings = [];
    this.exercisesWithSameName = [];
    this.exerciseChose[this.previewExerciseChosen] = false;
    this.exerciseChosen = true;
    this.exerciseChose[i] = true;
    this.previewExerciseChosen = i;
    this.viewChart = false;
    if (this.chart) {
      this.chart.destroy();
    }
    for (const j of this.exercises) {
      if (j.status === 1 && j.name.toLowerCase() === this.filtredExercises[this.previewExerciseChosen].name.toLowerCase()) {
        this.exercisesWithSameName.push(j);
      }
    }
    for (const j of this.exercisesWithSameName) {
      a.push(j.trainingID);
    }
    this.trainingService.getTrainingsById(a).subscribe(
      receivedTrainings => {
        this.trainings = receivedTrainings;
        if (this.trainings[0]) {
          this.viewChart = true;
          this.chart = new Chart(this.ctx, {
            // The type of chart we want to create
            type: 'line',
            // The data for our dataset
            data: {
              datasets: [{
                label: 'Objętość (kg)',
                borderColor: 'rgb(0, 191, 255)',
                color: 'white',
              }]
            },

            // Configuration options go here
            options: {
              responsive: true,
              legend: {
                labels: {
                  // This more specific font property overrides the global property
                  fontFamily: 'sans-serif',
                  fontColor: 'white',
                  fontSize: 20
                }
              },
              scales: {
                yAxes: [{
                  ticks: {
                    fontColor: 'white',
                    fontSize: 16,
                    beginAtZero: true
                  }
                }],
                xAxes: [{
                  ticks: {
                    fontColor: 'white',
                    fontSize: 16,
                  }
                }]
              },
              tooltips: {
                titleFontFamily: 'sans-serif',
                bodyFontFamily: 'sans-serif'
              }
            }
          });
          for (let k = 0; k < this.trainings.length; k++) {
              this.chart.data.labels[k] = this.trainings[k].date;
          }
          for (let k = 0; k < this.exercisesWithSameName.length; k++) {
            this.chart.data.datasets[0].data[k] = this.exercisesWithSameName[k].reps *
              this.exercisesWithSameName[k].series * this.exercisesWithSameName[k].weight;
          }
        } else {
          this.viewChart = false;
        }
      }
    );
  }
}

