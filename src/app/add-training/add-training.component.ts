import { ExerciseService } from './../service/exercise.service';
import { TrainingData } from './../class/training-data';
import { ExerciseData } from './../class/exercise-data';
import { Component, OnInit, HostListener } from '@angular/core';
import { TrainingService } from '../service/training.service';
import { FormGroup, Validators, FormBuilder, RequiredValidator, FormGroupDirective } from '@angular/forms';
import { Router, RouterOutlet, ActivatedRoute } from '@angular/router';
import { slider } from '../animations';

@Component({
  selector: 'app-add-training',
  templateUrl: './add-training.component.html',
  styleUrls: ['./add-training.component.css'],
  animations: [
    slider
  ]
})
export class AddTrainingComponent implements OnInit {

  value = 0;
  exerciseId: 0;
  trainingId: 0;
  resetForm = false;
  exerciseSucceed = 0;
  submittedExerciseForm = false;
  submittedTrainingForm = false;
  exerciseDeleted = false;
  exerciseIdToEdit = 0;
  training: TrainingData;
  exercise: ExerciseData;
  exercises: ExerciseData[] = [];
  exerciseForm: FormGroup;
  trainingForm: FormGroup;
  trainingIsBeingEdited = false;
  i = 0;

  constructor(
    private trainingService: TrainingService,
    private exerciseService: ExerciseService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  addExercise(name, series, reps, weight) {
    this.submittedExerciseForm = true;
    if (this.exerciseForm.invalid) {
      this.resetForm = false;
      return;
    }
    if (this.exerciseIdToEdit === 0) {
      this.exercises.push(new ExerciseData(this.exerciseId, name, reps, series, weight, this.trainingId, 0));
      this.exerciseService.addExercise(new ExerciseData(this.exerciseId, name, series, reps, weight, this.trainingId, 0)).subscribe();
      this.exerciseId++;
    } else {
      this.exercises.splice(this.i, 1);
      this.exercises.splice(this.i, 0, new ExerciseData(this.exerciseIdToEdit, name, series, reps, weight, this.trainingId, 0));
      this.exerciseService.addExercise(new ExerciseData(this.exerciseIdToEdit, name, series, reps, weight, this.trainingId, 0)).subscribe();
    }
    this.exerciseForm.reset();
    this.resetForm = true;
    this.exerciseIdToEdit = 0;
    this.exerciseSucceed = 0;
  }

  selected(value): void {
    this.value = value;
  }

  addTraining(name, date, startTime, endTime) {
    this.submittedTrainingForm = true;

    if (this.trainingForm.invalid) {
      return;
    }

    const newTraining = new TrainingData(this.trainingId, name, date, startTime, endTime);
    this.trainingService.addTraining(newTraining).subscribe(
      () => newTraining);

    this.router.navigate(['/']);
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params.name == null) {
        this.trainingForm = this.formBuilder.group({
          name: ['', Validators.required],
          date: ['', Validators.required],
          startTime: ['', Validators.required],
          endTime: ['', Validators.required]
        });
        this.trainingService.getTrainingId()
          .subscribe((data) => this.trainingId = data + 1);
        this.exerciseService.getExerciseId()
          .subscribe((data) => this.exerciseId = data + 1);
      } else {
        this.trainingIsBeingEdited = true;
        this.trainingForm = this.formBuilder.group({
          name: [params.name, Validators.required],
          date: [params.date, Validators.required],
          startTime: [params.startTime, Validators.required],
          endTime: [params.endTime, Validators.required]
        });
        this.trainingId = params.id;
        this.exerciseService.getExercises(this.trainingId).subscribe(
          receivedExercises => { this.exercises = receivedExercises; });
      }
    });
    this.exerciseForm = this.formBuilder.group({
      name: ['', Validators.required],
      series: ['', Validators.required],
      reps: ['', Validators.required],
      weight: ['', Validators.required]
    });
    this.exerciseService.getExerciseId()
      .subscribe((data) => this.exerciseId = data + 1);
  }

  get e() {
    return this.exerciseForm.controls;
  }

  get t() {
    return this.trainingForm.controls;
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  editExistingExercise(i) {
    this.exerciseForm = this.formBuilder.group({
      name: [this.exercises[i].name, Validators.required],
      series: [this.exercises[i].series, Validators.required],
      reps: [this.exercises[i].reps, Validators.required],
      weight: [this.exercises[i].weight, Validators.required]
    });
    this.exerciseIdToEdit = this.exercises[i].id;
    this.i = i;
    this.exerciseSucceed = this.exercises[i].status;
  }

  changeWeight(weight, i) {
    if (weight === '') {
      weight = 0;
    }
    console.log(weight);
    this.exerciseForm.get('weight').patchValue(weight + i);
  }

  deleteExercise(i) {
    this.exerciseService.deleteExercise(this.exercises[i].id).subscribe();
    this.exercises.splice(i, 1);
    this.exerciseDeleted = true;
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    if (this.trainingIsBeingEdited === false && this.submittedTrainingForm === false && this.exerciseDeleted === false) {
      this.exerciseService.deleteAllExercisesWithTrainingId(this.trainingId).subscribe();
    }
  }

  @HostListener('window:beforeunload', ['$event'])
  onRefresh(event) {
    if (this.trainingIsBeingEdited === false && this.submittedTrainingForm === false && this.exerciseDeleted === false) {
      this.exerciseService.deleteAllExercisesWithTrainingId(this.trainingId).subscribe();
    }
  }
}
